import os
import sys
import random
import signal
import asyncio
import logging

import numpy as np

from ligo_shmem import shmem


logger = logging.getLogger(__name__)

# FIXME: is this the right size to check?
FLOAT_SIZE = sys.getsizeof(np.float64)


# model mbuf buffers:
#
# <model>_daq: 67108864 2
# <model>: 67108864 4
# <model>_tp: 67108864 2
# <model>_awg: 67108864 2
# <model>_gds: 67108864 1
# ipc: 33554432 10
# shmipc: 16777216 5


class Model:
    def __init__(self, model_name, n_adc, n_dac):
        """initialize the userspace model

        """
        self.name = model_name

        logging.debug(f"initializing model {self.name}...")

        # FIXME: somehow determine model rate, number of ADC inputs
        # and DAC outputs from the model itself
        self.rate = 2046
        self.n_adc = n_adc
        self.n_dac = n_dac

        # FIXME: setup all the necessary shmem stuff, calculate all
        # offsets, allocate arrays for I/O, etc.
        # adc_args = (
        #     f'shm://{model_name}_adc',
        #     len_adc*FLOAT_SIZE/2**17,
        # )
        # logging.debug(f"creating ADC shmem: {adc_args}")
        # self.ADC = np.array(
        #     shmem(*adc_args),
        #     copy=False,
        # )
        # dac_args = (
        #     f'shm://{model_name}_dac',
        #     len_adc*FLOAT_SIZE,
        # )
        # logging.debug(f"creating DAC shmem: {dac_args}")
        # self.DAC = np.array(
        #     shmem(*dac_args),
        #     copy=False,
        # )
        self.input = np.zeros(self.n_adc)
        self.output = np.zeros(self.n_dac)

        self.sleep = random.random() * 5

        logging.debug(self)

    def __str__(self):
        return f'<Model {self.name}: {self.n_adc} inputs, {self.n_dac} outputs ({self.sleep})>'

    def start(self):
        """start model

        """
        # FIXME: somehow load the user code, launch the external
        # userspace process, etc.
        logging.debug(f"starting {self.name}...")

    def stop(self):
        """stop model

        """
        # FIXME: do we need to shut down any processes or clear out
        # any shm:// when we're done?
        logging.debug(f"stopping {self.name}...")

    async def run(self, in_data):
        """Run a single forward pass through the model1

        The input `in_data` should be a numpy array with length equal
        to the number of ADC inputs to the model.

        Returns a numpy array of DAC output values.

        """
        # copy the input (ADC) data into the shared memory block
        assert len(in_data) == len(self.input)
        self.input[:] = in_data

        # run model
        # FIXME: does the above insertion of data into the ADC buffer
        # alone trigger the model to run, or do we need to do
        # something else?
        # FIXME: how do we know the forward pass is complete? when the
        # DAC outputs are written?
        await asyncio.sleep(self.sleep)

        # write output
        # FIXME: should this be a copy or a reference?
        return self.output

##################################################

class IOP:
    def __init__(self):
        self.__run = True

    def stop(self, *args):
        self.__run = False

    async def run(self):
        model_plant = Model('my_plant_model', 10, 20)
        model_controller = Model('my_controller_model', 20, 10)

        assert len(model_plant.output) == len(model_controller.input)
        assert len(model_plant.input) == len(model_controller.output)

        inp = np.zeros_like(model_plant.input)
        inc = np.zeros_like(model_controller.input)

        logging.debug("starting models...")
        model_plant.start()
        model_controller.start()

        logging.debug("starting run loop...")
        while self.__run:
            logging.debug("loop")
            outputs = await asyncio.gather(
                model_plant.run(inp),
                model_controller.run(inc),
            )
            outp = outputs[0]
            outc = outputs[1]
            inp = outc
            inc = outp

        logging.debug("stopping models...")
        model_plant.stop()
        model_controller.stop()


async def main():
    iop = IOP()
    signal.signal(signal.SIGINT, iop.stop)
    await iop.run()


if __name__ == '__main__':
    logging.basicConfig(
        format="%(asctime)s %(message)s",
        level=os.getenv('LOG_LEVEL', 'DEBUG').upper(),
    )

    asyncio.run(main())
